﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SOM
{
    public partial class Form1 : Form
    {
        public SOM_1D som;
        public Trojkat trojkat;
        public Form1()
        {
            InitializeComponent();
            trojkat = new Trojkat();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if(trojkat.Punkt1 == default(Point))
            {
                trojkat.Punkt1 = new Point(e.X, e.Y);
                DrawPoint(e.X, e.Y, Brushes.Black);
                Console.WriteLine("Punkt 1 assigned " + trojkat.Punkt1.ToString());
            }
            else if(trojkat.Punkt2 == default(Point))
            {
                trojkat.Punkt2 = new Point(e.X, e.Y);
                DrawPoint(e.X, e.Y, Brushes.Black);
                Console.WriteLine("Punkt 2 assigned " + trojkat.Punkt2.ToString());
            }
            else if(trojkat.Punkt3 == default(Point))
            {
                trojkat.Punkt3 = new Point(e.X, e.Y);
                DrawPoint(e.X, e.Y, Brushes.Black);
                Console.WriteLine("Punkt 3 assigned " + trojkat.Punkt3.ToString());              
                DrawLines();
            }
        }

        private void DrawPoint(float x, float y, Brush brush)
        {
            Graphics g = pictureBox1.CreateGraphics();
            g.FillRectangle(brush, x, y, 3, 3);           
        }

        private void DrawLine(Point a, Point b)
        {
            Pen pen = Pens.Black;
            Graphics g = pictureBox1.CreateGraphics();
            g.DrawLine(pen, a.X, a.Y, b.X, b.Y);
        }

        private void DrawLines()
        {
            Pen pen = Pens.Black;
            Graphics g = pictureBox1.CreateGraphics();
            g.DrawLine(pen, trojkat.Punkt1.X, trojkat.Punkt1.Y, trojkat.Punkt2.X, trojkat.Punkt2.Y);
            g.DrawLine(pen, trojkat.Punkt1.X, trojkat.Punkt1.Y, trojkat.Punkt3.X, trojkat.Punkt3.Y);
            g.DrawLine(pen, trojkat.Punkt2.X, trojkat.Punkt2.Y, trojkat.Punkt3.X, trojkat.Punkt3.Y);
        }

        private void Start_Click(object sender, EventArgs e)
        {
            if (som == null || som.GetLiczbaNeruonow() == 0 || iteration_num.Text.Length == 0 || learningrate.Text.Length == 0)
            {
                MessageBox.Show("Musisz najpierw zainicjowac neurony i ustawić wszystkie opcje!", "Uwaga", MessageBoxButtons.OK);
                return;
            }
            som.Max_Iter = Convert.ToInt32(iteration_num.Text);
            som.LR = double.Parse(learningrate.Text, System.Globalization.CultureInfo.InvariantCulture); 
            som.Run2(UpdateCanvas);
        }

        private void NeuronInit_Click(object sender, EventArgs e)
        {
            if (trojkat == null || trojkat.IsReady())
            {
                MessageBox.Show("Musisz najpierw stworzyć trójkąt!", "Uwaga", MessageBoxButtons.OK);
                return;
            }
            if (neuron_num.Text.Length == 0)
            {
                MessageBox.Show("Musisz podać liczbę neuronów do inicjalizacji!", "Uwaga", MessageBoxButtons.OK);
                return;
            }
            int liczba_neuronow = Convert.ToInt32(neuron_num.Text);
            som = new SOM_1D(liczba_neuronow, trojkat);
            som.NeuronInit();
            var neurony = som.GetNeurony();

            for(int i = 0; i < neurony.Count-1; ++i)
            {
                if(i == 0)
                    DrawPoint(neurony[i].X, neurony[i].Y, Brushes.Black);
                DrawLine(neurony[i], neurony[i + 1]);
                DrawPoint(neurony[i + 1].X, neurony[i + 1].Y, Brushes.Black);
            }
            NeuronInit.Enabled = false;
        }

        public void UpdateCanvas(Point sygnał)
        {
            var neurony = som.GetNeurony();

            Iteration.Text = som.Iteration.ToString();
            LearningRateT.Text = som.LR.ToString();
            PromienSasiedztwa.Text = som.Promien_Sasiedztwa.ToString();

            Graphics g = pictureBox1.CreateGraphics();
            g.Clear(Color.White);
            Pen pen = Pens.Black;

            DrawPoint(sygnał.X, sygnał.Y, Brushes.Red);
            g.DrawLine(pen, trojkat.Punkt1.X, trojkat.Punkt1.Y, trojkat.Punkt2.X, trojkat.Punkt2.Y);
            g.DrawLine(pen, trojkat.Punkt1.X, trojkat.Punkt1.Y, trojkat.Punkt3.X, trojkat.Punkt3.Y);
            g.DrawLine(pen, trojkat.Punkt2.X, trojkat.Punkt2.Y, trojkat.Punkt3.X, trojkat.Punkt3.Y);

            for (int i = 0; i < neurony.Count - 1; ++i)
            {
                if (i == 0)
                    DrawPoint(neurony[i].X, neurony[i].Y, Brushes.Black);
                DrawLine(neurony[i], neurony[i + 1]);
                DrawPoint(neurony[i + 1].X, neurony[i + 1].Y, Brushes.Black);
            }
            
        }

    }
    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Point(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public static Point operator+(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point operator-(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public static Point operator*(Point a, double d)
        {
            return new Point(a.X * (float)d, a.Y * (float)d);
        }

        public static Point operator *(double d, Point a)
        {
            return new Point(a.X * (float)d, a.Y * (float)d);
        }

        public static Point operator +(Point a, double d)
        {
            return new Point(a.X * (float)d, a.Y * (float)d);
        }

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }

    public class Trojkat
    {
        public Point Punkt1 { get; set; }
        public Point Punkt2 { get; set; }
        public Point Punkt3 { get; set; }

        public Trojkat()
        { }

        public Trojkat(Point Punkt1, Point Punkt2, Point Punkt3)
        {
            this.Punkt1 = Punkt1;
            this.Punkt2 = Punkt2;
            this.Punkt3 = Punkt3;
        }

        public bool IsReady()
        {
            return Punkt1 == default(Point) || Punkt2 == default(Point) || Punkt3 == default(Point);
        }
    }

    public class SOM_1D
    {
        private Random random = new Random();
        private List<Point> Neurony = new List<Point>();      
        private int Liczba_Neuronow = 0;
        private Trojkat trojkat;
        public int Promien_Sasiedztwa { private set; get; }
        public int Max_Iter { get; set; }
        public double LR { get; set; }
        public delegate void UpdateCanvas(Point sygnal);
        private Timer timer = new Timer();
        public int Iteration { private set; get; }

        public SOM_1D(int Liczba_Neuronow, Trojkat trojkat)
        {
            this.Liczba_Neuronow = Liczba_Neuronow;
            this.trojkat = trojkat;
            Promien_Sasiedztwa = Liczba_Neuronow / 2;
            
        }

        public void NeuronInit()
        {
            for(int i = 0; i < Liczba_Neuronow; ++i)
            {
                Neurony.Add(LosujPunktNaTrojkacie());
            }
        }

        
        public Point LosujPunktNaTrojkacie()
        {
            
            float p1 = (float)random.NextDouble();
            float p2 = (float)random.NextDouble();
            if (p1 > p2)
            {
                var tmp = p1;
                p1 = p2;
                p2 = tmp;
            }

            float x = p1 * trojkat.Punkt1.X + (p2 - p1) * trojkat.Punkt2.X + (1 - p2) * trojkat.Punkt3.X;
            float y = p1 * trojkat.Punkt1.Y + (p2 - p1) * trojkat.Punkt2.Y + (1 - p2) * trojkat.Punkt3.Y;
            return new Point(x, y);
        }

        public List<Point> GetNeurony()
        {
            return Neurony;
        }

        public int GetLiczbaNeruonow()
        {
            return Liczba_Neuronow;
        }

        public async void Run2(UpdateCanvas updateCanvas)
        {
            double learning_rate_0 = LR, learning_rate = LR;
            int sigma_0 = Liczba_Neuronow / 2;
            double lambda = Max_Iter / Math.Log(sigma_0);
            
            int decrease_step = Max_Iter / Promien_Sasiedztwa;

            
            timer.Interval = 1;
            timer.Tick += Timer_Tick;

            for (int i = 0; i < Max_Iter; i++)
            {
                Iteration = i + 1;
                // zmniejszenie promieniu sąsiedztwa
                if ((i + 1) % decrease_step == 0)
                {
                    Promien_Sasiedztwa--;
                }

                // losowanie sygnału do przesunięcia
                var sygnal = LosujPunktNaTrojkacie();

                // obliczenie odległości euklidesowej między punktami
                List<double> odleglosci = new List<double>();
                foreach (var neuron in Neurony)
                {
                    var sum = Euclidean(sygnal, neuron);
                    odleglosci.Add(sum);
                }

                // ustalanie indeksu neuronu zwyciezcy oraz promieniu sąsiadów
                int index_najblizszego = odleglosci.IndexOf(odleglosci.Min());
                int first_index = index_najblizszego - Promien_Sasiedztwa < 0 ? 0 : index_najblizszego - Promien_Sasiedztwa;
                int last_index = index_najblizszego + Promien_Sasiedztwa > Liczba_Neuronow ? Liczba_Neuronow : index_najblizszego + Promien_Sasiedztwa;

                Neurony[index_najblizszego] = Neurony[index_najblizszego] + learning_rate * (sygnal - Neurony[index_najblizszego]);

                // przesuwanie neuronów
                for (int j = first_index; j < last_index; ++j)
                {
                    if (j == index_najblizszego) continue;
                    Neurony[j] = Neurony[j] + learning_rate / Math.Abs(index_najblizszego - j) * (sygnal - Neurony[j]);
                }
                timer.Start();
                await Task.Run(() => {
                    while (timer.Enabled) { }
                });

                updateCanvas(sygnal); // updating canvas

                double x = Math.Exp(-(i + 1) / lambda);
                learning_rate = learning_rate_0 * x;
                //learning_rate = 1.0 / Math.Sqrt(i+1.0);
                LR = learning_rate;
            }
        }

        public void Timer_Tick(object sender, EventArgs e)
        {

            timer.Stop();
        }

        public double Euclidean(Point A, Point B)
        {
            return Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
        }

    }
}
