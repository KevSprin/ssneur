﻿namespace SOM
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.neuron_num = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.iteration_num = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Start = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.learningrate = new System.Windows.Forms.TextBox();
            this.NeuronInit = new System.Windows.Forms.Button();
            this.Iteration = new System.Windows.Forms.Label();
            this.LearningRateT = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PromienSasiedztwa = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(477, 426);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // neuron_num
            // 
            this.neuron_num.Location = new System.Drawing.Point(532, 70);
            this.neuron_num.Name = "neuron_num";
            this.neuron_num.Size = new System.Drawing.Size(100, 20);
            this.neuron_num.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(532, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Liczba neuronów";
            // 
            // iteration_num
            // 
            this.iteration_num.Location = new System.Drawing.Point(532, 118);
            this.iteration_num.Name = "iteration_num";
            this.iteration_num.Size = new System.Drawing.Size(100, 20);
            this.iteration_num.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(532, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Liczba iteracji";
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(535, 324);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(75, 23);
            this.Start.TabIndex = 5;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(532, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Współczynnik uczenia";
            // 
            // learningrate
            // 
            this.learningrate.Location = new System.Drawing.Point(532, 168);
            this.learningrate.Name = "learningrate";
            this.learningrate.Size = new System.Drawing.Size(100, 20);
            this.learningrate.TabIndex = 7;
            // 
            // NeuronInit
            // 
            this.NeuronInit.Location = new System.Drawing.Point(647, 67);
            this.NeuronInit.Name = "NeuronInit";
            this.NeuronInit.Size = new System.Drawing.Size(75, 23);
            this.NeuronInit.TabIndex = 8;
            this.NeuronInit.Text = "Inicjuj";
            this.NeuronInit.UseVisualStyleBackColor = true;
            this.NeuronInit.Click += new System.EventHandler(this.NeuronInit_Click);
            // 
            // Iteration
            // 
            this.Iteration.AutoSize = true;
            this.Iteration.Location = new System.Drawing.Point(647, 118);
            this.Iteration.Name = "Iteration";
            this.Iteration.Size = new System.Drawing.Size(0, 13);
            this.Iteration.TabIndex = 9;
            // 
            // LearningRateT
            // 
            this.LearningRateT.AutoSize = true;
            this.LearningRateT.Location = new System.Drawing.Point(647, 171);
            this.LearningRateT.Name = "LearningRateT";
            this.LearningRateT.Size = new System.Drawing.Size(0, 13);
            this.LearningRateT.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(532, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Promień Sąsiedztwa:";
            // 
            // PromienSasiedztwa
            // 
            this.PromienSasiedztwa.AutoSize = true;
            this.PromienSasiedztwa.Location = new System.Drawing.Point(647, 209);
            this.PromienSasiedztwa.Name = "PromienSasiedztwa";
            this.PromienSasiedztwa.Size = new System.Drawing.Size(0, 13);
            this.PromienSasiedztwa.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PromienSasiedztwa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LearningRateT);
            this.Controls.Add(this.Iteration);
            this.Controls.Add(this.NeuronInit);
            this.Controls.Add(this.learningrate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.iteration_num);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.neuron_num);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox neuron_num;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox iteration_num;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox learningrate;
        private System.Windows.Forms.Button NeuronInit;
        private System.Windows.Forms.Label Iteration;
        private System.Windows.Forms.Label LearningRateT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label PromienSasiedztwa;
    }
}

