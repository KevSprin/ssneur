﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hopfield
{
    public partial class Form1 : Form
    {
        private static int n = 10;
        private static int N = n * n;
        Rectangle[,] grid = new Rectangle[n, n];
        int[,] InputGrid = new int[n, n];
        int[] InputArray = new int[N];
        //int[,] TestBitGrid = new int[n, n];
        double[,] Weights = new double[N, N];
        List<int[]> Patterns = new List<int[]>();
        private Graphics g;
        private int PatternCounter = 0;
        List<int> ShuffledIndicies = new List<int>();
        private bool StopFlag = false;
        private Random rand = new Random();

        public Form1()
        {
            InitializeComponent();
            g = pictureBox1.CreateGraphics();
            InitBitGrid();
            ShuffledIndicies.AddRange(Enumerable.Range(0, N));
            ShuffledIndicies = ShuffleList(ShuffledIndicies);
        }

        /// <summary>
        /// Inicjacja macierzy barw
        /// </summary>
        private void InitBitGrid()
        {
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    InputGrid[i, j] = -1;
                }
            }
        }

        /// <summary>
        /// Do tworzenia planszy kwadratów
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateGridButton_Click(object sender, EventArgs e)
        {

            Pen pen = Pens.Black;
            int y = 0, w = 50;
            for (int i = 0; i < n; ++i)
            {
                int x = 0;
                for (int j = 0; j < n; ++j)
                {
                    Rectangle rect = new Rectangle(x, y, w, w);
                    grid[i, j] = rect;
                    g.DrawRectangle(pen, rect);
                    x += w;
                }
                y += w;
            }
            CreateGridButton.Enabled = false;
            TrainButton.Enabled = true;
            ClearButton.Enabled = true;

        }

        /// <summary>
        /// Do wyboru pola
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            int x_coord = e.X;
            int y_coord = e.Y;
            Brush brush;

            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (grid[i, j].X < x_coord && grid[i, j].Y < y_coord && (grid[i, j].X + grid[i, j].Width) > x_coord && (grid[i, j].Y + grid[i, j].Height) > y_coord)
                    {
                        if (InputGrid[i, j] == -1)
                        {
                            brush = Brushes.Black;
                            g.FillRectangle(brush, grid[i, j]);
                            InputGrid[i, j] = 1;
                        }
                        else
                        {
                            brush = Brushes.White;
                            Pen pen = Pens.Black;
                            g.FillRectangle(brush, grid[i, j]);
                            g.DrawRectangle(pen, grid[i, j]);
                            InputGrid[i, j] = -1;
                        }
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Wyczyszczenie pola
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearButton_Click(object sender, EventArgs e)
        {
            InputGrid = new int[n, n];
            CreateGridButton.Enabled = true;
            InitBitGrid();
            RefreshGrid(InputGrid);
        }

        /// <summary>
        /// Do trenowania danego wzorca
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrainButton_Click(object sender, EventArgs e)
        {
            PatternCounter++;
            var bitArray = GridToArray(InputGrid);
            Patterns.Add(bitArray);
            PatternBox.Items.Add(bitArray);
            Train();
            LoadButton.Enabled = true;
            TestButton.Enabled = true;
            PatternBox.Enabled = true;
            ShuffleButton.Enabled = true;
        }

        /// <summary>
        /// Do przekształcenia planszy w ciąg bitów
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns></returns>
        private int[] GridToArray(int[,] Grid)
        {
            int index = 0;
            int[] result = new int[N];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    result[index] = Grid[i, j];
                    index++;
                }
            }
            return result;
        }

        /// <summary>
        /// Do przekształcenia z 1D array na 2D array
        /// </summary>
        /// <param name="Array"></param>
        /// <returns></returns>
        private int[,] ArrayToGrid(int[] Array)
        {
            int[,] result = new int[n, n];
            int array_counter = 0;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    result[i, j] = Array[array_counter];
                    array_counter++;
                }
            }
            return result;
        }

        /// <summary>
        /// Funkcja signum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private int Sign(double value)
        {
            return value >= 0 ? 1 : -1;
        }

        /// <summary>
        /// Trenowanie sieci hopfielda regułą hebbowską
        /// </summary>
        private void Train()
        {
            double[,] w = new double[N, N];
            for (int i = 0; i < N; ++i)
            {
                for (int j = 0; j < N; ++j)
                {
                    if (i == j) continue;
                    double sum = 0;
                    foreach (var pattern in Patterns)
                    {
                        sum += pattern[i] * pattern[j];
                    }
                    w[i, j] = 1.0/PatternCounter * sum;
                }
            }
            Weights = w;
        }

        /// <summary>
        /// Do testowania autoasocjacji sieci hopfielda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void TestButton_Click(object sender, EventArgs e)
        {
            //ShuffledIndicies = ShuffleList(ShuffledIndicies);
            StopButton.Enabled = true;
            TrainButton.Enabled = true;
            CreateGridButton.Enabled = true;
            TestButton.Enabled = false;
            Timer timer = new Timer();
            timer.Interval = 20;
            timer.Tick += delegate
            {
                timer.Stop();
            };

            var y = (int[])InputArray.Clone();

            while (!StopFlag)
            {
                foreach (var i in ShuffledIndicies)
                {
                    double sum = 0;
                    for (int j = 0; j < N; ++j)
                    {
                        if (i == j) continue;
                        sum += Weights[i, j] * y[j];
                    }
                    //sum += InputArray[i];
                    timer.Start();
                    y[i] = Sign(sum);
                    var pair = GetIndex2D(i);
                    if (y[i] >= 0)
                    {
                        Brush brush = Brushes.Black;
                        g.FillRectangle(brush, grid[pair.Item1, pair.Item2]);
                    }
                    else
                    {
                        Brush brush = Brushes.White;
                        Pen pen = Pens.Black;
                        g.FillRectangle(brush, grid[pair.Item1, pair.Item2]);
                        g.DrawRectangle(pen, grid[pair.Item1, pair.Item2]);
                    }
                    await Task.Run(() => {
                        while (timer.Enabled) { }
                    });

                }
            }
            TestButton.Enabled = true;
            StopButton.Enabled = false;
            StopFlag = false;
        }

        Tuple<int,int> GetIndex2D(int index)
        {
            int index_i = 0, index_j = 0;
            for(int i = 0; i < N; i += n)
            {
                if(index - i < n)
                {
                    index_j = index - i;
                    index_i = i / 10;
                    break;
                }
            }
            return new Tuple<int, int>(index_i, index_j);
        }

        /// <summary>
        /// Do wylosowania jakiejś planszy z której będzie robiona autoasocjacja
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShuffleButton_Click(object sender, EventArgs e)
        {        
            Brush brush_black = Brushes.Black;
            Brush brush_white = Brushes.White;
            Pen pen = Pens.Black;

            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (rand.Next(0,2) == 1)
                    {                        
                        InputGrid[i, j] = 1;
                    }
                }
            }

            InputArray = GridToArray(InputGrid);
            RefreshGrid(InputGrid);
            TrainButton.Enabled = false;
            CreateGridButton.Enabled = false;
            TestButton.Enabled = true;
        }

        /// <summary>
        /// Do odświeżenia planszy wedle wartości w tablicy
        /// </summary>
        /// <param name="Grid"></param>
        private void RefreshGrid(int[,] ColorGrid)
        {
            Pen pen = Pens.Black;
            Brush brush_black = Brushes.Black;
            Brush brush_white = Brushes.White;


            for(int i = 0; i < n; ++i)
            {
                for(int j = 0; j < n; ++j)
                {
                    if (ColorGrid[i, j] == -1)
                    {
                        g.FillRectangle(brush_white, grid[i, j]);
                        g.DrawRectangle(pen, grid[i, j]);
                    }
                    else
                    {
                        g.FillRectangle(brush_black, grid[i, j]);
                    }
                }
            }
        }

        /// <summary>
        /// Do wczytania wzorca z comboboxa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            if (PatternBox.SelectedItem == null) return;
            InputGrid = ArrayToGrid((int[])PatternBox.SelectedItem);
            RefreshGrid(InputGrid);
        }

        /// <summary>
        /// Mieszanie elementów listy
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="inputList"></param>
        /// <returns></returns>
        private List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            StopFlag = true;
        }
    }
}
